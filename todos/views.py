from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.db.models import Count
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todolist(request):
    todo_list = TodoList.objects.all().annotate(items_count=Count("items"))
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def update_todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            model_instance.id = form.cleaned_data["list"].id
            return redirect("todo_list_detail", id=model_instance.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/createItem.html", context)


def update_todoitem(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            model_instance.id = form.cleaned_data["list"].id
            return redirect("todo_list_detail", id=model_instance.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/editItem.html", context)
